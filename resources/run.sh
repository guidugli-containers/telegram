#!/bin/bash

## PARAMETERS ##

# Dont really dbus to have the browser working
ENABLE_DBUS=0

# Set timezone. By default use same as host system
TIMEZONE="$(timedatectl show --value -p Timezone)"

# Path where Firefox data will be stored
# Comment the line below to store all data inside container but
# all data will be lost after container execution
DATA_PATH=${HOME}/container-data/TelegramDesktop

# If DATA_PATH is commented, a new profile will be created and used,
# then deleted at the end of the session. Instead, if DATA_PATH is
# specified and DATA_PATH_DISCARD_ON_EXIT is set to 1 (one), the
# container will use the path specified by DATA_PATH but all changes
# will be discarded on exit
DATA_PATH_DISCARD_ON_EXIT=0

# Specify download folder location
# Comment the line below to store all data inside container but
# all data will be lost after container execution
DOWNLOAD_FOLDER=${HOME}/Downloads/Telegram

# Sometimes sharing hardware like webcams may need share udev
SHARE_UDEV_DATA=0

######################################################
CONTAINER_NAME="telegram"
CONTAINER_IMAGE="docker.io/guidugli/telegram:latest"

ARGS="$@"

# Get yubikey token device
YUDEV=
if command -v fido2-token &> /dev/null
then
  YUDEV=`fido2-token -L | cut -d: -f1`
fi

# Get user information
USER_UID=`id -u`
USER_GID=`id -g`

# Easy to move to docker or using sudo
PROG='podman'

# mandatory parameters
MANDATORY="--userns=keep-id --security-opt label=disable"

# Devices
DEVICES="--device /dev/snd"
DEVICES+=" --device /dev/dri"
if [ -n "$YUDEV" ]; then
  DEVICES+=" --device $YUDEV"
fi

# Detect video devices (webcam)
VIDEO_DEVICES=
for device in /dev/video*
do
  if [ -c $device ]; then
    DEVICES+=" --device $device"
  fi
done

# Environment variables
ENV_VARS="-e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native"
ENV_VARS+=" -e DISPLAY=$DISPLAY"
ENV_VARS+=" -e XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR"
ENV_VARS+=" -e WAYLAND_DISPLAY=$WAYLAND_DISPLAY"
ENV_VARS+=" -e USER_UID=$USER_UID"
ENV_VARS+=" -e USER_GID=$USER_GID"
ENV_VARS+=" -e USER_NAME=$(id -un)"

if [ -n "$ARGS" ]; then
  ENV_VARS+=" -e ARGS=$ARGS"
fi

if [ -n "${XAUTH}" ]; then
  ENV_VARS+=" -e XAUTHORITY=${XAUTH}"
fi

if [ -n "$TIMEZONE" ]; then
  ENV_VARS+=" -e TZ=$TIMEZONE"
fi

if [ $ENABLE_DBUS -eq 1 ]; then
  ENV_VARS+=" -e DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS"
fi

# Volumes to be mapped
VOLUMES="-v ${XDG_RUNTIME_DIR}/pulse:${XDG_RUNTIME_DIR}/pulse"
VOLUMES+=" -v /tmp/.X11-unix:/tmp/.X11-unix"
VOLUMES+=" -v /etc/machine-id:/etc/machine-id:ro"
VOLUMES+=" -v /dev/shm:/dev/shm"
VOLUMES+=" -v ${XDG_RUNTIME_DIR}/${WAYLAND_DISPLAY}:${XDG_RUNTIME_DIR}/${WAYLAND_DISPLAY}"

if [ -n "${DOWNLOAD_FOLDER}" ]; then
  mkdir -p ${DOWNLOAD_FOLDER} 2>/dev/null
# Line below does not work because of the space in the path (maybe a shell expert can help out).
#  VOLUMES+=" -v ${DOWNLOAD_FOLDER}:/home/$(id -un)/Telegram Desktop"
fi

if [ -n "${DATA_PATH}" ]; then
  VFLAG=':Z'
  if [ "$DATA_PATH_DISCARD_ON_EXIT" -eq 1 ]; then
    VFLAG=":O"
  fi

  mkdir -p ${DATA_PATH} 2>/dev/null
  VOLUMES+=" -v ${DATA_PATH}:/home/$(id -un)/.local/share/TelegramDesktop${FLAG}"
  VFLAG=
fi

if [ $ENABLE_DBUS -eq 1 ]; then
  VOLUMES+=" -v ${XDG_RUNTIME_DIR}/bus:${XDG_RUNTIME_DIR}/bus"
  VOLUMES+=" -v /run/dbus/system_bus_socket:/run/dbus/system_bus_socket"
fi

if [ $SHARE_UDEV_DATA -eq 1 ]; then
  VOLUMES+=" -v /run/udev/data:/run/udev/data:ro"
fi

### START ###

$PROG run --rm -it $MANDATORY $DEVICES $ENV_VARS $VOLUMES -v $DOWNLOAD_FOLDER:"/home/$(id -un)/Downloads/Telegram Desktop:Z" --group-add users -u root --name $CONTAINER_NAME $CONTAINER_IMAGE

