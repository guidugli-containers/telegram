# Telegram

Provide container image running Telegram messaging application. Also provides scripts to add icons to Gnome, making it ease to call. The scripts use podman as the container tool.

## Usage

Clone this repository and execute the install.sh to add the necessary files to have Brave icon to appear in Gnome desktop.

After install, the script can also be called by command line: `telegram_podman_run.sh`

All files are installed in the home directory of the user running the install script and it also executes as a regular user (rootless container), increasing isolation.

Customization can be made on `$HOME/.local/bin/telegram_podman_run.sh`:
- Change volume paths
- add/remove devices
- add/remove podman run parameters
- Change timezone

## License
BSD
