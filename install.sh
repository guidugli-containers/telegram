#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

mkdir -p $HOME/.local/share/icons &>/dev/null

cp -f $SCRIPT_DIR/icons/telegram-512.png $HOME/.local/share/icons/

cp -f $SCRIPT_DIR/resources/run.sh $HOME/.local/bin/telegram_podman_run.sh

chmod 700 $HOME/.local/bin/telegram_podman_run.sh

cat << EOF > $HOME/.local/share/applications/telegram_podman.desktop
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Telegram Podman
Comment=Web Messaging
Icon=$HOME/.local/share/icons/telegram-512.png
Exec=$HOME/.local/bin/telegram_podman_run.sh
Terminal=false
Categories=Tags;Describing;Application
EOF

chmod 600 $HOME/.local/share/icons/telegram-512.png $HOME/.local/share/applications/telegram_podman.desktop
