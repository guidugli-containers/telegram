#!/usr/bin/env bash

CONTAINER_NAME='telegram:latest'
USER_UID=$(id -u)
USER_GID=$(id -g)

CCMD=
# Check if to use podman or docker
if command -v podman &> /dev/null
then
 CCMD=`command -v podman`
else
 CCMD=`command -v docker`
fi

if [ -z "$CCMD" ]; then
  echo "ERROR: Could not find podman or docker"
  exit 1
fi

$CCMD rmi $CONTAINER_NAME &>/dev/null

$CCMD build --tag "$CONTAINER_NAME" \
	--rm --force-rm --compress \
	--build-arg USER_UID=$USER_UID \
	--build-arg USER_GID=$USER_GID \
	--no-cache=true .

